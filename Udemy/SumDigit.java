package CodingExercise;

public class SumDigit {

		public static int sumFirstAndLastDigit(int number)
		{
			int sum = 0;
			if(number > 0)
			{
				sum += number%10;
				while(number > 10)
				{
					number = number/10;
				}
				sum +=number;
				return sum;
			}
			return -1;
		}

		public static void main(String[] args) {
		 SumDigit sd =new SumDigit();
		 System.out.println(sd.sumFirstAndLastDigit(-123));

		}

	}
