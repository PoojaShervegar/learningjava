package CodingExercise;

public class SumEven {

			public static int getEvenDigitSum(int number)
			{
				int sum = 0, digit = 0;
				if(number > 0)
				{
					while(number > 0)
					{
						digit = number%10;
						number = number/10;
						if(digit%2 == 0)
						{
							sum += digit;
						}	
					}
					return sum;
				}
				return -1;
			}
	public static void main(String[] args) {
		SumEven se = new SumEven();
		System.out.println(se.getEvenDigitSum(23456));

	}

}
