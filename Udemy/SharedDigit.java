package CodingExercise;

public class SharedDigit {

				public static boolean hasSharedDigit(int number1, int number2)
				{
					int digit1 = 0, digit2 = 0;
					if(number1 >=10 && number1 <=99 && number2 >=10 && number2 <=99)
					{
							digit1 = number1%10;
							number1 = number1/10;
							digit2 = number2%10;
							number2 = number2/10;
							if(digit1 == digit2 || digit1 == number2 || number1 == digit2 || number1 == number2)
							{
							return true;	
							}
					}
					return false;
				}
		public static void main(String[] args) {
			SharedDigit sd = new SharedDigit();
			System.out.println(sd.hasSharedDigit(10, 21));

		}

	}
