package CodingExercise;

public class GCD {
	public static int getGreatestCommonDivisor(int first, int second)
	{
		int div1, div2, num=0;
		if(first >=10 && second >=10)
		{
			if(first <= second)
			{
				div1 = first;
				div2 = second;
			}
			else 
				{
				div1 = second;
				div2 = first;
				}
			
			for (int i =2; i<=div1; i++)
			{
				if(div1 % i == 0)
				{
					if(div2 % i == 0)
					{						
						if(i > num)
						{
							num=i;
							
						}
					}
				}
				
			}
			return num;
		}
		return -1;
	}

	public static void main(String[] args) {
		GCD gcd =new GCD();
		System.out.println(gcd.getGreatestCommonDivisor(30, 12));
	}

}
