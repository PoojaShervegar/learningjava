package CodingExercise;

public class LeapYearDayOfMonth {
	static public boolean isLeapYear(int year)
	{
		if(year>=1 && year<=9999)
		{
			if((year % 4 == 0 && year % 100 != 0 ) || year % 400 == 0)
			{
				return true;
			}
		}
			return false;
	}
	public static int getDaysInMonth(int month, int year)
	{
		if(month >=1 && month <=12)
		{
			if(year>=1 && year<=9999)
			{
				boolean isLeap = isLeapYear(year);
				switch(month)
				{
				case 1:
					return 31;
				case 2:
					if(isLeap == true)
						return 29;
					else
						return 28;
				case 3:
					return 31;
				case 4:
					return 30;
				case 5:
					return 31;
				case 6:
					return 30;
				case 7:
					return 31;
				case 8:
					return 30;
				case 9:
					return 31;
				case 10:
					return 30;
				case 11:
					return 31;
				case 12:
					return 30;
					default:
				return -1;
						
			}
		}
	}
		return -1;
	}
	static public void main(String[] args)
	{
		LeapYearDayOfMonth lp = new LeapYearDayOfMonth();
		System.out.println(LeapYearDayOfMonth.isLeapYear(2000));
		System.out.println(lp.getDaysInMonth(2, 2020));
	}
}
