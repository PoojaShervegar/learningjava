package CodingExercise;

public class SameLastDigit {

					public static boolean hasSameLastDigit(int number1, int number2, int number3)
					{
						int digit1 = 0, digit2 = 0, digit3 = 0;
						if(number1 >=10 && number1 <=1000 && number2 >=10 && number2 <=1000 && number3 >=10 && number3 <=1000)
						{
								digit1 = number1%10;
								digit2 = number2%10;
								digit3 = number3%10;
								if(digit1 == digit2 || digit1 == digit3 || digit2 == digit3)
								{
								return true;	
								}
						}
						return false;
					}
			public static void main(String[] args) {
				SameLastDigit sld = new SameLastDigit();
				System.out.println(sld.hasSameLastDigit(12, 22, 54));

			}

		}
