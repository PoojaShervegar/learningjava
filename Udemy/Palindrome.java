package CodingExercise;

public class Palindrome {
	public static boolean isPalindrome(int number)
	{
		int reverse = 0, num = number;
		if(number > 0)
		{
			while(number > 0)
			{
				int i = number % 10;
				number = number/10;
				reverse = reverse*10 + i;
			}
		}
		if(reverse == num )
		{
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
	 Palindrome p =new Palindrome();
	 System.out.println(p.isPalindrome(-1221));

	}

}
