package CodingExercise;

public class OddCheck {
	public static boolean isOdd(int number)
	{
		if(number >0)
		{
			if(number % 2 != 0)
				return true;
		}
		return false;
	}
	public static int sumOdd(int start, int end)
	{
		int sum=0;
		if(start > 0 && end > 0 && end >=start)
		{
			for(int i = start; i<=end; i++)
			{
				if(isOdd(i))
				{
					sum += i;
				}
			}
			return sum;
		}
		return -1;
	}
	public static void main(String[] args) {
		OddCheck od = new OddCheck();
		System.out.println(od.isOdd(11));
		System.out.println(od.isOdd(12));
		System.out.println(od.sumOdd(1, 11));

	}

}
