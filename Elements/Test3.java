package Elements;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Test3 {
	static WebDriver driver;
	public void AllData()
	{
		for(int row=1; row<=10; row++)
		{
			for(int col=1; col<=7; col++)
			{
				System.out.println(driver.findElement(By.cssSelector("#content > div > div > div > div.large-10.columns > table > tbody > tr:nth-child("+row+") > td:nth-child("+col+")")).getText());
			}
		}
	}
	public void specificData(int row, int col)
	{
		System.out.println(driver.findElement(By.cssSelector("#content > div > div > div > div.large-10.columns > table > tbody > tr:nth-child("+row+") > td:nth-child("+col+")")).getText());
	}
	public void ifDynamicAllData()
	{
		WebElement table = driver.findElement(By.cssSelector("#content > div > div > div > div.large-10.columns > table"));
		List <WebElement> row=table.findElements(By.tagName("tr"));
		for(int r=0; r<row.size();r++)
		{
			List <WebElement> col=row.get(r).findElements(By.tagName("th"));
			for(int c=0; c<col.size(); c++)
			{
				System.out.println(col.get(c).getText());
			}
			List <WebElement> data = row.get(r).findElements(By.tagName("td"));
			for(int d=0; d<data.size(); d++)
			{
				System.out.println(data.get(d).getText());
			}
		}
	}
	public static void main(String[] args) {
		
		String Url = "http://the-internet.herokuapp.com/challenging_dom";
		System.setProperty("webdriver.gecko.driver", "D:/Eclipse Neon/geckodriver-v0.21.0-win64/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.get(Url);
		Test3 t3=new Test3();
		//t3.AllData();
	//	t3.specificData(4, 5);
		t3.ifDynamicAllData();
	}

}
