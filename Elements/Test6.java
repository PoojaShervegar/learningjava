package Elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Test6 {

	public static void main(String[] args) {
	String Url = "http://the-internet.herokuapp.com/disappearing_elements";
	System.setProperty("webdriver.gecko.driver", "D:/Eclipse Neon/geckodriver-v0.21.0-win64/geckodriver.exe");
	WebDriver driver = new FirefoxDriver();
	driver.get(Url);
	WebElement home= driver.findElement(By.linkText("Home"));
	WebElement about = driver.findElement(By.linkText("About"));
	home.click();
	System.out.println("home displayed "+home.isDisplayed());
	driver.navigate().back();
	driver.navigate().back();
	about.click();
	System.out.println("About displayed "+about.isDisplayed());

	}

}
