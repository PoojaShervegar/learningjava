package Elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Test8 {

	public static void main(String[] args) {
		String Url="http://the-internet.herokuapp.com/dropdown";
		System.setProperty("webdriver.gecko.driver","D:/Eclipse Neon/geckodriver-v0.21.0-win64/geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get(Url);
		WebElement element = driver.findElement(By.id("dropdown"));
		Select dropdown = new Select(element);
		dropdown.selectByIndex(2);
		dropdown.selectByValue("1");
		dropdown.selectByVisibleText("Option 1");
		List<WebElement> options=dropdown.getAllSelectedOptions();
		System.out.println(dropdown.getFirstSelectedOption().getText());
		for(int i=0; i<options.size(); i++)
		{
			System.out.println(options.get(i).getText());
		}
	}

}
