package Elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Test7 {
	public static void main(String[] args)
	{
String Url="http://the-internet.herokuapp.com/drag_and_drop";
System.setProperty("webdriver.gecko.driver","D:/Eclipse Neon/geckodriver-v0.21.0-win64/geckodriver.exe");
WebDriver driver = new FirefoxDriver();
driver.get(Url);
WebElement source = driver.findElement(By.id("column-a"));
WebElement dest = driver.findElement(By.id("column-b"));
Actions action = new Actions(driver);
//action.dragAndDrop(source, dest).build().perform();
//action.dragAndDrop(dest, source).perform();
action.clickAndHold(source).moveToElement(dest).release(dest).build().perform();

	}
}
