package Elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Test4 {

	public static void main(String[] args) {
		String Url = "http://the-internet.herokuapp.com/checkboxes";
		System.setProperty("webdriver.gecko.driver", "D:/Eclipse Neon/geckodriver-v0.21.0-win64/geckodriver.exe");
		FirefoxDriver driver = new FirefoxDriver();
		driver.get(Url);
		String pageTitle = driver.findElement(By.cssSelector("#content > div > h3")).getText();
		System.out.println("Page Title: "+pageTitle);
		WebElement checkbox1=driver.findElement(By.cssSelector("#checkboxes > input[type='checkbox']:nth-child(1)"));
		checkbox1.click();
		List<WebElement> checkbox = driver.findElements(By.cssSelector("#checkboxes > input[type='checkbox']"));
		Boolean check1,check2;
		check1 = checkbox.get(0).isSelected();
		check2 = checkbox.get(1).isSelected();
		if(check1=true)
		checkbox.get(0).click();
		else if(check2=true)
		checkbox.get(1).click();

	}

}
